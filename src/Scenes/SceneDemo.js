import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from './../Button';
import { Card } from './../Card';
import { CardSection } from './../CardSection';

export default class SceneDemo extends Component {
	onReactNativeVideoPress() {
		console.log('demo: react-native-video');
		Actions.sceneReactNativeVideo();
	}

	onReactNativeYoutubePress() {
		console.log('demo: react-native-youtube');
		Actions.sceneReactNativeYoutube();
	}

	onMorePress() {
		console.log('not implemented');
	}

	render() {
		return (
			<ScrollView style={{ paddingTop: 10, flex: 1 }}>
				<Card>
					<CardSection>
						<Button onPress={this.onReactNativeVideoPress.bind(this)}>
							react-native-video
						</Button>
					</CardSection>
					<CardSection>
						<Button onPress={this.onReactNativeYoutubePress.bind(this)}>
							react-native-youtube
						</Button>
					</CardSection>
					<CardSection>
						<Button danger onPress={this.onMorePress.bind(this)}>
							More
						</Button>
					</CardSection>
				</Card>
			</ScrollView>
		);
	}
}
