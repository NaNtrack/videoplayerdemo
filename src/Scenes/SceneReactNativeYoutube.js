import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import YouTube from 'react-native-youtube';

export default class SceneReactNativeYoutube extends Component {
	state = {
		isReady: false,
		status: null,
		quality: null,
		error: null,
		isPlaying: true
	}

	render() {
		return (
			<View style={styles.container}>
				<YouTube
					videoId="dQw4w9WgXcQ"
					play={this.state.isPlaying}
					onReady={() => { this.setState({ isReady: true }); }}
					onChangeState={(e) => { this.setState({ status: e.state }); }}
					onChangeQuality={(e) => { this.setState({ quality: e.quality }); }}
					onError={(e) => { this.setState({ error: e.error }); }}
					style={{ alignSelf: 'stretch', height: 300, backgroundColor: 'black', marginVertical: 10 }}
					apiKey='AIzaSyBkPFUwswZsvqEA6ofeXFe9D9REs6gwXdI'
				/>

				<TouchableOpacity onPress={() => { this.setState((s) => { return { isPlaying: !s.isPlaying }; }); }}>
					<Text style={[styles.welcome, { color: 'blue' }]}>{this.state.status === 'playing' ? 'Pause' : 'Play'}</Text>
				</TouchableOpacity>

				<Text style={styles.instructions}>{this.state.isReady ? 'Player is ready.' : 'Player setting up...'}</Text>
				<Text style={styles.instructions}>Status: {this.state.status}</Text>
				<Text style={styles.instructions}>Quality: {this.state.quality}</Text>
				<Text style={styles.instructions}>{this.state.error ? 'Error: ' + this.state.error : ' '}</Text>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		//backgroundColor: '#F5FCFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
};
