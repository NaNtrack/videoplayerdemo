import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import SceneDemo from './Scenes/SceneDemo';
import SceneReactNativeVideo from './Scenes/SceneReactNativeVideo';
import SceneReactNativeYoutube from './Scenes/SceneReactNativeYoutube';

class RouterComponent extends Component {
	render() {
		return (
			<Router sceneStyle={styles.sceneStyles}>
				<Scene key="main">
					<Scene key="edit" component={SceneDemo} title="Video Player Demo" initial />
					<Scene key="sceneReactNativeVideo" component={SceneReactNativeVideo} title="React Native Video - Demo" />
					<Scene key="sceneReactNativeYoutube" component={SceneReactNativeYoutube} title="React Native Youtube - Demo" />
				</Scene>
			</Router>
		);
	}
}

const styles = {
	sceneStyles: {
		paddingTop: 64
	}
};

export default RouterComponent;
