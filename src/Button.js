import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ children, onPress, danger, success }) => {
	const {
		buttonDanger,
		buttonSuccess,
		buttonNormal,
		textDanger,
		textSuccess,
		textNormal
	} = styles;

	return (
		<TouchableOpacity
			style={
				danger ?
					buttonDanger :
					(success ?
						buttonSuccess :
						buttonNormal
					)
			}
			onPress={onPress}
		>
			<Text
				style={
					danger ?
						textDanger :
						(success ?
							textSuccess :
							textNormal
						)
				}
			>
			{children}
			</Text>
		</TouchableOpacity>
	);
};

const buttons = {
	style: {
		flex: 1,
		alignSelf: 'stretch',
		borderRadius: 5,
		borderWidth: 1,
		marginLeft: 5,
		marginRight: 5,
		height: 50
	},
	success: {
		backgroundColor: '#449d44',
		borderColor: '#398439'
	},
	danger: {
		backgroundColor: '#c9302c',
		borderColor: '#ac2925'
	},
	normal: {
		backgroundColor: '#fff',
		borderColor: '#007aff'
	}
};

const texts = {
	style: {
		alignSelf: 'center',
		fontSize: 16,
		fontWeight: '600',
		paddingTop: 15,
		paddingBottom: 10
	},
	success: {
		color: '#fff'
	},
	danger: {
		color: '#fff'
	},
	normal: {
		color: '#007aff',
	}
};

const styles = {
	buttonNormal: { ...buttons.style, ...buttons.normal },
	buttonSuccess: { ...buttons.style, ...buttons.success },
	buttonDanger: { ...buttons.style, ...buttons.danger },
	textNormal: { ...texts.style, ...texts.normal },
	textSuccess: { ...texts.style, ...texts.success },
	textDanger: { ...texts.style, ...texts.danger },
};

export { Button };
